# AI Package
This is a helper library for the Neurorace project, can be used with [Neuroracer Robot](https://gitlab.com/NeuroRace/neuroracer-robot.git) 
or [Neuroracer Simulator](https://gitlab.com/NeuroRace/neuroracer-sim.git). It was written to improve the usability and 
allows developers to interact with a simple api instead of different complex backend functions.

It is mainly intended to be used in combination with Neurorace Project. The neuroracer-ai package is used to create, load
and predict with ai models.

__IMPORTANT__:  
neuroracer-ai cannot train any ai models, it can only load them and predict with them. The main purpose of 
this package is to load already trained models and make predictions. __Neuroracer Robot__ and __Neuroracer Simulation__ are
using this package to load all different kinds of models and make predictions. It can load all different types of 
available ai models (like [Neuroracer AI SL](https://gitlab.com/NeuroRace/neuroracer-ai-sl.git) or 
[Neuroracer AI RL](https://gitlab.com/NeuroRace/neuroracer-ai-rl.git)).

At the moment it is only available through this repository. In near future, it will be available by pip (pypi).

<table>
  <tr>
   <td width="100%" valign="top">

## Content
- [Python Version](#python-version)
- [Installation](#installation)
- [Backend](#backend)
- [Usage](#usage)
  - [Jupyter Notebooks](#jupyter-notebooks)
  - [Integration into the NeuroRacer / Simulation](#integration-into-the-neuroracer-simulation) <!-- don't touch link is working -->
  - [Training](#training)
    </td>
  </tr>
</table>

## Python Version
Due to [ROS](http://wiki.ros.org/Documentation) dependencies of other packages, which are based on this one, both - Python 2.7 and Python3.x - are supported.

As soon as ROS fully supports Python3.x, the Python2.7 support will be dropped.

## Installation

To install the library clone the repository and use `pip install -e`. The command will install the package in editable mode.
To install in both python environments, call pip specifically.

```
git clone https://gitlab.com/NeuroRace/neuroracer-ai.git
pip2 install --user -e neuroracer-ai[backend]
pip3 install --user -e neuroracer-ai[backend]
```

where `backend` can be either [Keras](https://github.com/keras-team/keras) with [Tensorflow](https://github.com/tensorflow/tensorflow) or [PyTorch](https://github.com/pytorch/pytorch). The available install options are
* `keras_tf` (Keras with Tensorflow backend - cpu only)
* `keras_tf_gpu` (Keras with Tensorflow backend - gpu support)
* `torch` (dynamically selects cpu/gpu)

So for example if you want to install the PyTorch backend for python3.x, the command would be
```
pip3 install --user -e neuroracer-ai[torch]
```
Of course you can install Keras and PyTorch side by side and switch via config file any time. See [Backend](#backend) section for further information.

## Backend
To switch the used backend, edit `$HOME/.neurorace/neuroracer.yaml`

* Keras
```yaml
{backend: keras}
```

* PyTorch
```yaml
{backend: pytorch}
```

**`Note:`**  
The folder and config file will be created on first execution with __*keras*__ as default. You can predefine the file. Set either __*keras*__ or __*pytorch*__ as backend.

### Jupyter Notebooks
To understand how to use the api for a trained model, the following jupyter notebooks offer some guidance.

* [How to load a trained AI](notebooks/01_how_to_load_trained_ai.ipynb)
* [How to predict with an AI](notebooks/02_how_to_predict.ipynb)
* [How to debug an AI](notebooks/03_how_to_debug_ai.ipynb)

**Note:**  
Creating an ai is not listed as it is only possible by one of the training packages ([Supervised Learning](https://gitlab.com/NeuroRace/neuroracer-ai-sl) or [Reinforcement Learning](https://gitlab.com/NeuroRace/neuroracer-ai-rl)). This is a principle by design since it does not make sense to create an ai which is randomly initialized and not able to be trained.

### Integration into the NeuroRacer / Simulation
For setting up the AI with the [`neuroracer-robot`](https://gitlab.com/NeuroRace/neuroracer-robot) or [`neuroracer-sim`](https://gitlab.com/NeuroRace/neuroracer-sim), see the setup section in the according repository.

### Training
To train a model for execution provided by this package, use the [Supervised Learning](https://gitlab.com/NeuroRace/neuroracer-ai-sl) or [Reinforcement Learning](https://gitlab.com/NeuroRace/neuroracer-ai-rl) module. 
