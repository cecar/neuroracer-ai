import numpy as np
from neuroracer_common.decorators import measure_time

from neuroracer_ai.processors import ImageProcessor
from neuroracer_ai.suites import AbstractProcessorSuite

PROGRESS_DESC = "Processing 2D Images"


class Image2DProcessorSuite(AbstractProcessorSuite):
    """
    A ProcessorSuite which processes images with an internal ImageProcessor
    instance. Parameters for the processor can be set in the constructor.

    It requires the data to be iterable!
    """

    def __init__(self, image_processor_params=None):
        AbstractProcessorSuite.__init__(self)
        self._image_processor = ImageProcessor(params=image_processor_params)

    @measure_time(immediate=False, verbose=0)
    def process(self, data):
        """
        Processes one or more Images per data point with the ImageProcessor
        input shape should be (n_samples, n_images_per_sample, height, width,
        n_color_channels). Multiprocessing is not easily available, as the
        process method is class method and cannot be pickled in python 2.7
        in order to be sent to multiprocessing pool workers. This requires
        overriding pickling or Python 3. Alternatively, processing has to
        to be refactored into module level functions.

        :param data: Input images. Every sample can have one or multiple images
        :type data: np.array
        :return: processed images
        :rtype: np.array
        """
        # If every sample has multiple images
        # print data.shape
        if not len(data.shape) == 5:
            raise ValueError("Input must be of shape (n_samples, n_images_per_sample, height, width, n_color_channels)"
                             "\nInput has shape {}".format(data.shape))

        if data.shape[1] > 1:  # if we have more than one image per sample
            return self._process_multi_input(data)

        else:
            n_samples, n_images, height, width, channels = data.shape
            unprocessed_images = data.reshape(-1, height, width, channels)
            processed_images = np.array([self._image_processor.process(image) for image in unprocessed_images])

            return processed_images

    def _process_multi_input(self, data):
        """
        Processes arrays containing more than one image per sample by
        reshaping the array before and after processing.

        input shape must be:
             (n_samples, n_images_per_sample, height, width, n_color_channels)

        output shape will be a list of array with shape:
             (n_images_per_sample, n_samples, height, width, n_color_channels)

        :param data: Input Images
        :type data: np.array
        :return: processed Images
        :rtype: np.array
        """

        n_samples, n_images_per_sample, height, width, n_color_channels = data.shape
        flat_images = data.reshape((-1, 1, height, width, n_color_channels))
        processed = self.process(flat_images)
        
        return np.split(processed, n_images_per_sample, axis=0)
