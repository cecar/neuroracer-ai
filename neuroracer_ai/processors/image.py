from __future__ import division

import cv2
import numpy as np
from flufl.enum import Enum
from neuroracer_ai.processors.abstract import AbstractProcessor
from neuroracer_ai.utils.backend_solver import get_backend
from neuroracer_common.decorators import measure_time
from neuroracer_ai.factories.abstract import EBackends

BACKEND = get_backend()


class ImageResizeParameters:
    """
    Parameters which define how the image gets resized.
    """

    def __init__(self, x_scaling, y_scaling, interpolation_method=cv2.INTER_AREA, relative_scaling=True):
        """
        :param x_scaling: x scaling either in percent or absolute pixels
        :type x_scaling: float
        :param y_scaling: y scaling either in percent or absolute pixels
        :type: y_scaling: float
        :param interpolation_method: OpenCV interpolation method int
        :param relative_scaling: defines how the scaling values are
                interpreted. If true scaling values are interpreted as
                percentage (0..1) otherwise as absolute pixels
        :type relative_scaling: bool
        """

        self.x_scaling = x_scaling
        self.y_scaling = y_scaling
        self.interpolation_method = interpolation_method
        self.relative_scaling = relative_scaling


class ImageFlip(Enum):
    """
    Available options to flip an image.
    """

    vertical = 0
    horizontal = 1
    horizontal_and_vertical = -1


class ImageCropParameters:
    """
    Parameters for the cropping of images.
    """

    def __init__(self, top=0, bottom=0, left=0, right=0, relative=True):
        """
        :param top: amount of cropping at the top
        :type top: Union[int, float]
        :param bottom: amount of cropping at the bottom
        :type bottom: Union[int, float]
        :param left: amount of cropping at the left
        :type left: Union[int, float]
        :param right: amount of cropping at the right
        :type right: Union[int, float]
        :param relative: defines if the values should be interpreted as
                relative or absolute
        :rtype: bool
        """

        self.top = top
        self.bottom = bottom
        self.left = left
        self.right = right
        self.relative = relative


class ImageProcessingParameters:
    """
    Parameters which are used for the processing of the images. If a parameter
    is set to None it means that this function is disabled.
    """

    def __init__(self, resize_parameters=None,
                 opencv_color_space_conv_func=None, flip_option=None,
                 crop_parameters=None, normalize=None, decode_image=False,
                 subtract_mean=None):
        """
        :param resize_parameters: parameters that should be used for the
                rescaling. If None no rescaling will be applied
        :type resize_parameters: ImageResizeParameters
        :param opencv_color_space_conv_func: id of the OpenCV color conversion
                function which should be applied.
        :type opencv_color_space_conv_func: int
        :param flip_option: flip variant that should be used. If None, the
               image will not be flipped
        :type flip_option: ImageFlip
        :param crop_parameters: parameters that define the cropping of the
                image. If None the image will not be cropped
        :type crop_parameters: ImageCropParameters
        :param normalize: normalizes the image by dividing through the
                          provided value or broadcasted array
        :type normalize: float or np.array
        :param subtract_mean: zero centers the image by subtracting the
                              provided value or broadcasted array
        :type normalize: float or np.array
        :param decode_image: defines if the images must be decoded in the
                             beginning.
        :type decode_image: bool

        """

        self.resize_parameters = resize_parameters
        self.opencv_color_space_conv_func = opencv_color_space_conv_func
        self.flip_option = flip_option
        self.crop_parameters = crop_parameters
        self.normalize = normalize
        self.decode_image = decode_image
        self.subtract_mean = subtract_mean


class ImageProcessor(AbstractProcessor):
    """
    Modifies images with the configured methods.
    """

    def __init__(self, params):
        """
        :param params: Parameters which should be used for the image processing
        :type params: neuroracer_ai.processors.image.ImageProcessingParameters
        """

        super(ImageProcessor, self).__init__()

        self._raw_params = params  # type: ImageProcessingParameters

        self._flip_option = params.flip_option  # type: ImageFlip
        self._color_space_conv_func = params.opencv_color_space_conv_func  # type: int
        self._resize_parameters = params.resize_parameters  # type: ImageResizeParameters
        self._crop_parameters = params.crop_parameters  # type: ImageCropParameters
        self._use_normalization = params.normalize  # float or np.array
        self._decode_image = params.decode_image  # type: bool
        self._subtract_mean = params.subtract_mean  # Cstr

    @measure_time(immediate=False, verbose=0)
    def process(self, image):
        """
        Processes the given image with the configured options.

        :param image: image to manipulate
        :type image: np.ndarray
        :return: the modified image
        :rtype: np.ndarray
        """

        if self._decode_image:
            decoded = self._decode(image)
            # if image is already decoded,
            # self._decode will return None
            image = decoded if decoded else image

        if self._flip_option:
            image = self._flip(image)

        if self._color_space_conv_func:
            image = self._change_color_space(image)

        # should always be before resizing! otherwise the dimension are altered
        if self._crop_parameters:
            image = self._crop(image)

        # should always be after cropping! otherwise the dimension are altered
        if self._resize_parameters:
            image = self._resize(image)

        if self._subtract_mean is not None:
            image = self._subtract_from_image(image, self._subtract_mean)

        if self._use_normalization is not None:
            image = self._normalize(image, self._use_normalization)

        # workaround for greyscale images. otherwise OpenCV produces a image
        # without a depth dim
        if len(image.shape) == 2:
            image = np.expand_dims(image, axis=2)

        if BACKEND == EBackends.PYTORCH:
            # need to switch channels for PyTorch from (height, width, depth) to (depth, height, width)
            image = np.reshape(image, (image.shape[2], image.shape[0], image.shape[1]))

        return image

    @staticmethod
    def _decode(image):
        """
        Decodes the image as defined in its meta data.

        :param image: encoded OpenCV image
        :type image: np.ndarray
        :return: decoded image
        :rtype: np.ndarray
        """
        return cv2.imdecode(image, flags=cv2.IMREAD_UNCHANGED)

    def _flip(self, image):
        """
        Flips the image based on the params passed to the constructor.

        :param image: OpenCV image
        :type image: np.ndarray
        :return: modified image
        :rtype: np.ndarray
        """

        return cv2.flip(image, self._flip_option.value)

    def _resize(self, image):
        """
        Resizes the image based on the params passed to the constructor.

        :param image: OpenCV image
        :type image: np.ndarray
        :return: modified image
        :rtype: np.ndarray
        """

        x_scaling = self._resize_parameters.x_scaling
        y_scaling = self._resize_parameters.y_scaling
        interpolation_method = self._resize_parameters.interpolation_method
        destination_size = None
        if not self._resize_parameters.relative_scaling:
            destination_size = (x_scaling, y_scaling)

        return cv2.resize(image, dsize=destination_size, fx=x_scaling, fy=y_scaling,
                          interpolation=interpolation_method)

    def _change_color_space(self, image):
        """
        Changes the color space of the image based on the params passed to the
        constructor.

        :param image: OpenCV image
        :type image: np.ndarray
        :return: modified image
        :rtype: np.ndarray
        """

        return cv2.cvtColor(image, self._color_space_conv_func)

    def _crop(self, image):
        """
        Crops the image based on the params passed to the constructor.

        :param image: OpenCV image
        :type image: np.ndarray
        :return: modified image
        :rtype: np.ndarray
        """

        width = image.shape[1]
        height = image.shape[0]

        top = self._crop_parameters.top
        bottom = self._crop_parameters.bottom
        left = self._crop_parameters.left
        right = self._crop_parameters.right

        if self._crop_parameters.relative:
            top = int(top * height)
            bottom = int(bottom * height)
            left = int(left * width)
            right = int(right * width)

        return image[top:height - bottom, left:width - right]

    @staticmethod
    def _subtract_from_image(image, mean_image):
        """
        Subtracts the provided mean from the image. The mean can be a single
        value or an array.

        :param image: OpenCV image
        :type image: np.ndarray
        :param mean_image: the mean to be subtracted from the image
        :type mean_image: float | np.ndarray
        :return:
        """
        return np.subtract(image, mean_image, dtype=np.float)

    @staticmethod
    def _normalize(image, normalization_factor):
        """
        Normalizes the image by dividing through the normalization factor

        :param image: OpenCV image
        :type image: np.ndarray
        :param normalization_factor: normalization factor used to normalize the image
        :type normalization_factor: float | np.ndarray
        :return: modified image
        :rtype: np.ndarray
        """
        return np.divide(image, normalization_factor, dtype=np.float)


# workaround to get nested classes working with loading and dumping to yaml
ImageProcessingParameters.ResizeParameters = ImageResizeParameters
ImageProcessingParameters.CropParameters = ImageCropParameters
ImageProcessingParameters.Flip = ImageFlip
