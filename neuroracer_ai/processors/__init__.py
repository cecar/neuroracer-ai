"""
Contains AbstractProcessor implementations which are used by the
neuroracer_ai.suites package.

A user should never implement its own AbstractProcessor! As this causes
problems with the saving of the ai.
"""

from neuroracer_ai.processors.abstract import AbstractProcessor
from neuroracer_ai.processors.image import ImageProcessingParameters, ImageProcessor

__all__ = [
    "AbstractProcessor",
    "ImageProcessingParameters",
    "ImageProcessor"
]
