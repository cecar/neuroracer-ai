"""
This module is everything a user has to import to use the library. The main api
class is AI. It contains every available call. The other classes are all helper
classes to simplify the configuration.
"""

import numpy as np

from neuroracer_ai.factories import BackendFactory
from neuroracer_ai.models import Debuggable
from neuroracer_ai.utils import file_handler as fh


class AI:
    def __init__(self, model, name, model_dir, processor_suite):
        """
        Constructor of the AI class. Should never be called outside of the ai
        class. It should be considered private!

        :param model: model instance that is used for prediction and training
        :type model: neuroracer_ai.models.AbstractModel
        :param name: name of which the ai/ model uses to save.
        :type name: str
        :param model_dir: directory in which the model lives
        :type model_dir: str
        :param processor_suite: processing suite which processes every input
                for training and prediction
        :type processor_suite: neuroracer_ai.suites.AbstractProcessorSuite
        """

        ERROR_TXT = "{} parameter can't be None! If you called this " \
                    "constructor manually use the load or create method " \
                    "instead!"

        if model is None:
            raise ValueError(ERROR_TXT.format("model"))
        if name is None:
            raise ValueError(ERROR_TXT.format("name"))
        if model_dir is None:
            raise ValueError(ERROR_TXT.format("model_dir"))
        if processor_suite is None:
            raise ValueError(ERROR_TXT.format("processor_suite"))

        self._model = model  # type: models.AbstractModel
        self._name = name  # type: str
        self._model_dir = model_dir  # type: str
        self._processor_suite = processor_suite  # type: suites.AbstractProcessorSuite

    def get_processor_suite(self):
        """
        Return Processor Suite

        :return: suites.Class
        """
        return self._processor_suite

    @staticmethod
    def create(name, model_dir, architecture_func, architecture_func_params, is_regression,
               processor_suite):
        """
        Creates a new model and based on it a new working instance of the ai
        class.

        Required to be overridden by all subclasses.

        :param name: name of which the ai/ model uses to save
        :type name: str
        :param model_dir: directory in which the model lives
        :type model_dir: str
        :param architecture_func: architecture which is used by the model
        :type architecture_func: Architecture
        :param architecture_func_params: parameters which are passed to the
                architecture constructor
        :type architecture_func_params: Dict[str, Any]
        :param is_regression: Whether the problem is regression or classification based
        :type is_regression: bool
        :param processor_suite: processing suite which processes every input
                for training and prediction. If you need something fancier than
                the predefined ones you can use the
                neuroracer_ai.suites.ManualMappingProcessorSuite. It allows you
                to completely customize the routing of the data to the
                specified processors. You should never implement your own
                ProcessorSuite class! This will cause a failure while loading
                the model next time.
        :type processor_suite: neuroracer_ai.suites.AbstractProcessorSuite
        :return: constructed AI instance
        :rtype: neuroracer_ai.AI
        """
        raise NotImplementedError

    @staticmethod
    def load(name, model_dir, trainable=False):
        """
        Loads the given model and returns a working instance of the ai class.
        Setting trainable to false decreases the memory usage significantly!
        But should only be used if you only want to predict with an already
        trained nn.
        If you try to train an untrainable nn a Error will be thrown!

        :param: name: name of the ai/ model
        :type name: str
        :param model_dir: directory in which the model lives
        :type model_dir: str
        :param trainable: loads the model in a way which allows it to be
                          trained.
        :type trainable: bool
        :return: constructed AI instance
        :rtype: neuroracer_ai.AI
        """

        ai_parameters = fh.load_ai_parameters(fh.create_config_path(model_dir, name))

        model = BackendFactory.build()
        model.load(model_path=fh.create_model_path(model_dir, name), trainable=trainable)

        return AI(
            name=name,
            model_dir=model_dir,
            model=model,
            **ai_parameters)

    def save(self, directory, overwrite=True):
        """
        Saves the model and the ai config int the given dir.

        :param directory: directory to save
        :type directory: str
        :param overwrite: manages if model files should be overwritten
        :type overwrite: bool
        """

        self._write_config(directory=directory, overwrite=overwrite)
        self._model.save(
            model_path=fh.create_model_path(directory, self._name),
            overwrite=overwrite)

    def predict(self, input_data, verbose=0):
        """
        Predicts the values for drive and steering based on the given data.
        If a model does not predict a drive  speed value, a constant of 1
        is substituted

        :param input_data: a numpy array which contains the input data. The
                           structure of the array depends on the nn.
        :type input_data: numpy.ndarray
        :param verbose: sets the verbosity level of the model
                        0 = silent
                        1 = progress bar
                        2 = one line per epoch
        :type verbose: int
        :return: a float tuple with the predicted values for drive and steering
        :rtype: Tuple[float, float]
        """
        # convert to numpy array to access shape
        if isinstance(input_data, list):
            input_data = np.array(input_data)

        # we need shape (n_samples, n_images_per_sample, height, width, n_color_channels)
        if len(input_data.shape) == 4:
            input_data = np.expand_dims(input_data, axis=0)

        processed = self._processor_suite.process(input_data)
        predicted = self._model.predict(input_data=processed, verbose=verbose)

        if not self._model._is_regression:
            # TODO implement generic mapping function which needs to be defined/referenced
            #  by each classification network
            pass

        # TODO move somewhere else, so AI is independent
        #  (probably write some wrapper or move to both engine modules -sim+robot)
        # add constant drive speed value of 1 if no drive speed was predicted
        predicted = [(pred[0], 1) if len(pred) == 1 else pred for pred in predicted]

        return predicted

    def visualize_activation(self, layer_name=None, layer_idx=None, filter_indices=None,
                             seed_input=None):
        """
        Visualizes the activation of the nn and returns it as a numpy array.

        :param layer_name: name of the layer to visualize (only name or idx is
                needed, if both is supplied idx has priority)
        :type layer_name: str
        :param layer_idx: index of the layer to visualize (only name or idx is
                needed, if both is supplied idx has priority)
        :type layer_idx: int
        :param filter_indices: list of indices from filters which get
                visualized. if none all are visualized
        :type filter_indices: List[int]
        :param seed_input: list of images which help the visualization to
                converge
        :type seed_input: np.ndarray
        :return: visualization
        :rtype: np.ndarray
        :raises TypeError: if the backend doesn't support visualization
        """

        if not isinstance(self._model, Debuggable):
            raise TypeError("The chosen backend doesn't support visualization!")

        if seed_input is not None:
            seed_input = self._processor_suite.process(data=seed_input)

        return self._model.visualize_activation(layer_name=layer_name, layer_idx=layer_idx,
                                                filter_indices=filter_indices,
                                                seed_input=seed_input)

    def visualize_saliency(self, layer_name=None, layer_idx=None, filter_indices=None,
                           seed_input=None):
        """
        Visualizes the saliency of the nn and returns it as a numpy array.

        :param layer_name: name of the layer to visualize (only name or idx is
                needed, if both is supplied idx has priority)
        :type layer_name: str
        :param layer_idx: index of the layer to visualize (only name or idx is
                needed, if both is supplied idx has priority)
        :type layer_idx: int
        :param filter_indices: list of indices from filters which get
                visualized. if none all are visualized
        :type filter_indices: List[int]
        :param seed_input: list of images which help the visualization to
                converge
        :type seed_input: np.ndarray
        :return: visualization
        :rtype: np.ndarray
        :raises TypeError: if the backend doesn't support visualization
        """

        if not isinstance(self._model, Debuggable):
            raise TypeError("The chosen backend doesn't support visualization!")

        if seed_input is not None:
            seed_input = self._processor_suite.process(data=seed_input)

        return self._model.visualize_saliency(layer_name=layer_name, layer_idx=layer_idx,
                                              filter_indices=filter_indices,
                                              seed_input=seed_input)

    def get_weights(self, layer_name=None, layer_idx=None):
        """
        Retrieves the weights from a certain layer.

        :param layer_name: name of the layer. if both layer_idx and layer_name
                are given layer_name has priority.
        :param layer_idx: index of the layer
        :return: weights of the layer
        :rtype: np.ndarray
        """

        if not isinstance(self._model, Debuggable):
            raise TypeError("The chosen backend doesn't support visualization!")

        return self._model.get_weights(layer_name=layer_name, layer_idx=layer_idx)

    def get_summary(self):
        """
        Constructs the summary of the structure and returns it as a string.

        :return: summary string of the model
        :rtype: str
        """

        if not isinstance(self._model, Debuggable):
            raise TypeError("The chosen backend doesn't support visualization!")

        return self._model.get_summary()

    def _write_config(self, directory, overwrite):
        """
        Helper method to write the internal state into a config file.
        It prepares the data for the file_handler.create_config_path method

        :param directory:
        :param overwrite:
        """

        # TODO maybe we should make this use some list which contains the field that should get serialized?

        data = {
            "ai_parameters": {
                "processor_suite": self._processor_suite
            }
        }

        fh.write_ai_config(
            config_path=fh.create_config_path(directory, self._name),
            overwrite=overwrite,
            data=data)
