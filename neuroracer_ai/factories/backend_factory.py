import sys

from neuroracer_ai.utils.backend_solver import get_backend
from neuroracer_ai.factories.abstract import AbstractBackendFactory, EBackends


class BackendFactory(AbstractBackendFactory):
    """
    Backend factory to create base model instances
    """

    def __init__(self):
        super(BackendFactory, self).__init__()

    @staticmethod
    def build():
        """
        Build method to get an implementation of a backend

        :return: backend instance
        :type:   BaseModel
        """

        backend = get_backend()

        # checks if base keras backend
        if backend == EBackends.KERAS:
            from neuroracer_ai.models.keras_base_backend import KerasBaseModel
            from keras import __version__

            model = KerasBaseModel()

            sys.stderr.write('[INFO] Using Keras v' + str(__version__) + ' as backend.\n')

        # checks if base pytorch backend
        elif backend == EBackends.PYTORCH:
            from neuroracer_ai.models.pytorch_base_backend import PyTorchBaseModel
            from torch import __version__

            model = PyTorchBaseModel()

            sys.stderr.write('[INFO] Using PyTorch v' + str(__version__) + ' as backend.\n')

        # if none, raise ValueError
        else:
            raise ValueError(
                "couldn't create backend: {}".format(backend))

        sys.stderr.write('[INFO] This Model is for EXECUTION ONLY.\n')

        return model
