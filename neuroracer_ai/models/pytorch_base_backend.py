from __future__ import division, print_function

from os.path import expanduser, isfile

import torch as t
from torchsummary import summary

from neuroracer_ai.models.abstract import AbstractModel, Debuggable

CUDA = "cuda"
CPU = "cpu"


class PyTorchBaseModel(AbstractModel, Debuggable):
    """
    PyTorch based implementation.

    NOTE: THIS MODEL CLASS ONLY SUPPORTS EXECUTIVE TASKS; NOT USABLE FOR TRAINING
    """

    def __init__(self):
        super(PyTorchBaseModel, self).__init__()
        # common attributes
        self._internal_model = None  # type: torch.nn.Model
        self._is_regression = True

        # PyTorch specific
        self._optimizer = None  # type: torch.optim
        self._criterion = None  # inheritance of type: torch.nn.modules.loss._Loss

        # check for gpu otherwise use cpu
        self._device_to_use = t.device(CUDA) if t.cuda.is_available() else t.device(CPU)

    def create(self, architecture_func, architecture_func_params, is_regression=True, print_summary=True):
        """
        Creates a new model based on the given architecture_func and its
        parameters.

        :param architecture_func: architecture construction function
        :type architecture_func: Callable[[**kwargs], keras.Model]
        :param architecture_func_params: parameters which get passed to to the
                architecture construction function
        :type architecture_func_params: Dict[str, Any]
        :param is_regression: Whether the problem is regression or classification based
        :type is_regression: bool
        :param print_summary: defines if the summary of the model gets printed
                              at the end
        :type print_summary: bool
        :return: created Model instance
        :rtype: KerasBaseModel
        """

        self._internal_model = architecture_func(**architecture_func_params)
        self._internal_model.to(device=self._device_to_use)

        self._is_regression = is_regression

        if print_summary:
            # TODO actually should return a string object which needs to be call with print()
            #  but prints directly for now
            self.get_summary()

        return self

    def save(self, model_path, overwrite=True):
        """
        Save model.

        Derived from

        https://pytorch.org/tutorials/beginner/saving_loading_models.html#what-is-a-state-dict
        and
        https://medium.com/udacity-pytorch-challengers/saving-loading-your-model-in-pytorch-741b80daf3c

        :param model_path: TODO
        :param overwrite: TODO
        :return: TODO
        """

        checkpoint = {
            'model': self._internal_model,
            'state_dict': self._internal_model.state_dict(),
            'optim_dict': self._optimizer.state_dict()
        }

        model_path = expanduser(model_path)

        if overwrite or (not overwrite and not isfile(model_path)):
            t.save(checkpoint, model_path)

    def load(self, model_path, trainable=True):
        """
        Load model.

        Derived from

        https://pytorch.org/tutorials/beginner/saving_loading_models.html#what-is-a-state-dict
        and
        https://medium.com/udacity-pytorch-challengers/saving-loading-your-model-in-pytorch-741b80daf3c

        :param model_path: TODO
        :param trainable: TODO
        :return: TODO
        """
        try:
            model_path = expanduser(model_path)

            checkpoint = t.load(model_path, map_location=self._device_to_use)

            self._internal_model = checkpoint['model']
            self._internal_model.load_state_dict(checkpoint['state_dict'])

            if trainable:
                # only makes sense to load these parameters when model is trainable
                self._optimizer = self._internal_model.get_optimizer()
                self._optimizer.load_state_dict(checkpoint['optim_dict'])
                self._criterion = self._internal_model.get_loss_function()
            else:
                # dynamically remove dropout, etc. from model
                for parameter in self._internal_model.parameters():
                    parameter.requires_grad = False

                self._internal_model.eval()

            self._internal_model.to(device=self._device_to_use)

            return self

        except ValueError as e:
            raise IOError(
                "Trying to load the model ({}) failed!\nMessage: {}".format(model_path, e))

    def predict(self, input_data, verbose=0):
        """
        Predicts output based on the model.

        :param input_data: TODO
        :type input_data: numpy.ndarray
        :param verbose: sets the verbosity level of the model 0 = silent,
                1 = progress bar, 2 line per epoch
        :type verbose: int
        :return: TODO
        :rtype: numpy.ndarray
        """
        if not t.is_tensor(input_data):
            # TODO use static variable initialized with zeros and just change the values and send it to device,
            #  since shape is always the same?
            input_data = t.tensor(input_data, dtype=t.float, device=self._device_to_use)

        prediction = self._internal_model(input_data)

        if verbose > 0:
            print(prediction)

        return prediction

    def visualize_activation(self, layer_name=None, layer_idx=None, filter_indices=None, seed_input=None):
        """
        Visualizes the activation of the nn and returns it as a numpy array.

        :param layer_name: name of the layer to visualize (only name or idx is
                needed, if both is supplied idx has priority)
        :type layer_name: str
        :param layer_idx: index of the layer to visualize (only name or idx is
                needed, if both is supplied idx has priority)
        :type layer_idx: int
        :param filter_indices: list of indices from filters which get
                visualized. if none all are visualized
        :type filter_indices: List[int]
        :param seed_input: list of images which help the visualization to
                converge
        :type seed_input: np.ndarray
        :return: visualization
        :rtype: np.ndarray
        """
        # TODO: PLACEHOLDER
        pass

    def visualize_saliency(self, layer_name=None, layer_idx=None, filter_indices=None, seed_input=None):
        """
        Visualizes the saliency of the nn and returns it as a numpy array.

        :param layer_name: name of the layer to visualize (only name or idx is
                needed, if both is supplied idx has priority)
        :type layer_name: str
        :param layer_idx: index of the layer to visualize (only name or idx is
                needed, if both is supplied idx has priority)
        :type layer_idx: int
        :param filter_indices: list of indices from filters which get
                visualized. if none all are visualized
        :type filter_indices: List[int]
        :param seed_input: list of images which help the visualization to
                converge
        :type seed_input: np.ndarray
        :return: visualization
        :rtype: np.ndarray
        """
        # TODO: PLACEHOLDER
        pass

    def get_weights(self, layer_name=None, layer_idx=None):
        """
        Retrieves a layer's weights based on either its name (unique) or index.

        If `name` and `index` are both provided, `index` will take precedence.

        Each layer has two entries, first one for weights, second for bias,
        so we just use a step size of two for looping through the model's layers.

        Structure example of a layer:
        layer_1.weight
        layer_1.bias
        layer_2.weight
        ...

        :param layer_name:  TODO
        :param layer_idx:  TODO
        :return:  TODO
        """

        # each indexed layer consists of two entries, first weights, second bias.
        # So only count layer_count/2 is the total amount of individual layers.
        n_layers = int(sum(1 for _ in self._internal_model.named_parameters()) / 2)

        if layer_idx is not None:
            for idx, (name, weights) in enumerate(self._internal_model.named_parameters()):
                if idx % 2 == 0 and int((idx / 2)) + 1 == layer_idx:
                    # convert tensor to numpy array and return values
                    return weights.cpu().detach().numpy()

            raise ValueError('Was asked to retrieve layer at index ' +
                             str(layer_idx) + ' but model only has ' +
                             str(n_layers) + ' layers.')
        else:
            if not layer_name:
                raise ValueError('Provide either a layer name or layer index.')

        for idx, (name, weights) in enumerate(self._internal_model.named_parameters()):
            if idx % 2 == 0 and layer_name in name:
                # convert tensor to numpy array and return values
                return weights.cpu().detach().numpy()

        raise ValueError('No such layer: ' + layer_name)

    def get_summary(self):
        """
        Retrieves a model's summary.

        :return:  TODO
        """
        # TODO actually should return an object but prints directly for the moment
        summary(self._internal_model, input_size=self._internal_model.input_shape)
